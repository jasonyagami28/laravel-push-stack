<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function form(){
        return view('form');
    }


    public function welcome_sanbercode_post(Request $request){  
        //dd($request->all());
        $nama_depan=$request["nama_depan"];
        $nama_belakang=$request["nama_belakang"];
        return view('welcome_page', compact('nama_depan'), compact('nama_belakang'));
    }
}
