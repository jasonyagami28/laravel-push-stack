<!DOCTYPE html>
<html>
    <head>
        <title>SanberBook Form</title>
        <meta charset="UTF-8">
    </head>

    <body>
        <div>
            <h1>Make New Account</h1>
            <h2>Sign Up Form</h2>
        <div>
    <form action="welcome_page" method="POST">

    @csrf
    
        <fieldset>
            <legend style="font-size:16px">Must be Filled In Completely</legend>
                    <label for="user_first_name" style="font-size:14px">First name: </label><br>
                        <input type="text" placeholder="First Name" value="" id="user_first_name" name="nama_depan" required>
                        <br></br>
                    <label for="user_last_name" style="font-size:14px">Last name: </label><br>
                        <input type="text" placeholder="Last Name" value="" id="user_last_name" name="nama_belakang" required>
                        <br> </br>
            
            <label style="font-size:14px">Gender: </label><br>
                <input type="radio" name="gender_user" value="1" required="required">Male<br>
                <input type="radio" name="gender_user" value="2" required="required">Female<br>
                <input type="radio" name="gender_user" value="3" required="required">Other<br></br>

            <label style="font-size:14px">Nationality</label><br>
                 <select required>
                    <option value="">None</option>

                    <optgroup label="South East Asia Countries">
                    <option value="Indonesian">Indonesian</option>
                    <option value="Malaysia">Malaysia</option>
                    <option value="Singapore">Singapore</option>
                    <option value="Thailand">Thailand</option>
                    </optgroup>

                    <optgroup label ="Europe Countries">
                    <option value="England">England</option>
                    <option value="Netherland">Netherland</option>
                    <option value="France">France</option>
                    <option value="Germany">Germany</option>
                    </optgroup>
                </select>
        <br></br>

            <!--untuk required checkbox, memerlukan javascript. tapi saya belum mengerti javascript-->
            
            <label style="font-size:14px">Spoken Language </label><br>
                <label> <input type="checkbox" name="spoken_language" id="indonesian" value="1" >Indonesian<br></label>
                <label><input type="checkbox" name="spoken_language" id="English" value="2" >English<br></label>
                <label><input type="checkbox" name="spoken_language" id="Mandarin" value="3" >Mandarin<br></label>
                <label><input type="checkbox" name="spoken_language" id="Other" value="4" >Other<br></br></label>

            <label for="bio" style="font-size:14px">Bio</label><br>
                <textarea cols="40"rows="10" id="bio" required></textarea>
        <br></br>

        <input type="Submit" value="Sign Up">
        </fieldset>
    </form>
<br>
<p>&#169; Jason Terry</p>

    </body>
</html>